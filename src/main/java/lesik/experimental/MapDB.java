package lesik.experimental;

import org.mapdb.DB;
import org.mapdb.DBMaker;

import java.io.File;
import java.util.*;
import java.util.concurrent.ConcurrentNavigableMap;

/**
 * Created by lesik_ia on 26.06.2015.
 */
public class MapDB {

    public static void main(String [] args) {
        DB db = DBMaker.newFileDB(new File("db/" + UUID.randomUUID().toString() + ".db")).make();

        Map<String, Set<String>> map = db.getHashMap("data");
        map.put("1", new HashSet<String>(Arrays.asList("2", "3")));

        db.commit();
        System.out.println(map.get("1"));
        db.close();
    }

}
