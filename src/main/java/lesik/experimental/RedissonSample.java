package lesik.experimental;

import io.netty.util.concurrent.Future;
import org.redisson.Redisson;
import org.redisson.core.RMap;
import org.redisson.core.RQueue;
import org.redisson.core.RSet;

import javax.swing.text.StyledEditorKit;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by lesik_ia on 26.06.2015.
 */
public class RedissonSample {

    public static void main(String [] args) {
        // connects to default Redis server 127.0.0.1:6379
        Redisson redisson = Redisson.create();

        RMap<String, Set<String>> map = redisson.getMap("anyMap");
        Set<String> prevObject = map.put("123", new HashSet<String>(Arrays.asList("1", "2")));
        Set<String> currentObject = map.putIfAbsent("323", new HashSet<String>(Arrays.asList("1", "3")));
        Set<String> obj = map.remove("123");

        map.fastPut("321", new HashSet<String>(Arrays.asList("1", "2")));
        map.fastRemove("321");

        Future<Set<String>> putAsyncFuture = map.putAsync("321", new HashSet<String>(Arrays.asList("4")));
        Future<Boolean> fastPutAsyncFuture = map.fastPutAsync("321", new HashSet<String>(Arrays.asList("5")));

        map.fastPutAsync("321", new HashSet<String>(Arrays.asList("6")));
        System.out.println(map.get("321"));
        map.fastRemoveAsync("321");

        RSet<Set<String>> set = redisson.getSet("anySet");
        //set.add("xxx");

        //RQueue

        redisson.shutdown();
    }

}
