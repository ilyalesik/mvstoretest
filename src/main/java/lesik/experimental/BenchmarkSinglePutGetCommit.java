package lesik.experimental;

import jdbm.RecordManager;
import jdbm.RecordManagerFactory;
import org.h2.mvstore.MVMap;
import org.h2.mvstore.MVStore;
import org.mapdb.DB;
import org.mapdb.DBMaker;
import org.openjdk.jmh.annotations.*;
import org.openjdk.jmh.runner.Runner;
import org.openjdk.jmh.runner.RunnerException;
import org.openjdk.jmh.runner.options.Options;
import org.openjdk.jmh.runner.options.OptionsBuilder;
import org.redisson.Redisson;
import org.redisson.core.RMap;

import java.io.File;
import java.io.IOException;
import java.util.*;

/**
 * Hello world!
 *
 */
@State(Scope.Thread)
public class BenchmarkSinglePutGetCommit
{

    //MValue
    MVStore s = null;
    MVMap<Integer, Set<String>> mvMap = null;

    //h
    Map<Integer, Set<String>> hMap = null;

    //jdbm2
    RecordManager jdbmRecordManager = null;
    Map<Integer, Set<String>> jdbmMap = null;

    //MapDB
    DB mapDB = null;
    Map<Integer, Set<String>> mapDBmap = null;

    //redisson
    Redisson redisson;

    Map<Integer, Set<String>> redissonMap;

    @Setup
    public void init() throws IOException {
        // open the store (in-memory if fileName is null)
        s = MVStore.open("db/" + UUID.randomUUID().toString() + ".db");

        // create/get the map named "data"
        mvMap = s.openMap("data");

        hMap = new HashMap<Integer, Set<String>>();

        jdbmRecordManager = RecordManagerFactory.createRecordManager("db/" + UUID.randomUUID().toString() + ".db");
        jdbmMap = jdbmRecordManager.hashMap("data");

        mapDB = DBMaker.newFileDB(new File("db/" + UUID.randomUUID().toString() + ".db")).make();
        mapDBmap = mapDB.getHashMap("data");

        redisson = Redisson.create();

        redissonMap = redisson.getMap("data");
    }

    @TearDown
    public void stop() throws IOException {
        s.close();
        jdbmRecordManager.close();
        mapDB.close();
        redisson.shutdown();
    }

    @Benchmark
    public void testMValue() {
        Set<String> data = new HashSet<String>(Arrays.asList("Hello", "jmh"));
        mvMap.put(data.hashCode(), data);
        mvMap.get(data.hashCode());
        s.commit();
    }

    @Benchmark
    public void testJDBM() throws IOException {
        Set<String> data = new HashSet<String>(Arrays.asList("Hello", "jmh"));
        jdbmMap.put(data.hashCode(), data);
        jdbmMap.get(data.hashCode());
        jdbmRecordManager.commit();
    }

    @Benchmark
    public void testMapDB() throws IOException {
        Set<String> data = new HashSet<String>(Arrays.asList("Hello", "jmh"));
        mapDBmap.put(data.hashCode(), data);
        mapDBmap.get(data.hashCode());
        mapDB.commit();
    }


    @Benchmark
    public void testH() {
        Set<String> data = new HashSet<String>(Arrays.asList("Hello", "jmh"));
        hMap.put(data.hashCode(), data);
        hMap.get(data.hashCode());
    }

    @Benchmark
    public void testRedisson() {
        Set<String> data = new HashSet<String>(Arrays.asList("Hello", "jmh"));
        redissonMap.put(data.hashCode(), data);
        redissonMap.get(data.hashCode());
        redisson.flushdb();
    }

    public static void main( String[] args ) throws RunnerException {
        Options opt = new OptionsBuilder()
                .include(BenchmarkSinglePutGetCommit.class.getSimpleName())
                .forks(1)
                .build();

        new Runner(opt).run();
    }
}
