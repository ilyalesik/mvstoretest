package lesik.experimental;

import jdbm.PrimaryTreeMap;
import jdbm.RecordManager;
import jdbm.RecordManagerFactory;

import java.io.IOException;
import java.util.UUID;

/**
 * Created by lesik_ia on 26.06.2015.
 */
public class Jdbm {

    public static void main(String [] args) throws IOException {
        /** create (or open existing) database */
        String fileName = "db/" + UUID.randomUUID().toString() + ".db";
        RecordManager recMan = RecordManagerFactory.createRecordManager(fileName);

        /** Creates TreeMap which stores data in database.
         *  Constructor method takes recordName (something like SQL table name)*/
        String recordName = "firstTreeMap";
        PrimaryTreeMap<Integer,String> treeMap = recMan.treeMap(recordName);

        /** add some stuff to map*/
        treeMap.put(1, "One");
        treeMap.put(2, "Two");
        treeMap.put(3, "Three");

        System.out.println(treeMap.keySet());
        // > [1, 2, 3]

        /** Map changes are not persisted yet, commit them (save to disk) */
        recMan.commit();

        System.out.println(treeMap.keySet());
        recMan.close();
    }

}
